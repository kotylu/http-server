package dev.kotylu;

import dev.kotylu.model.request.RawRequestParser;
import dev.kotylu.model.request.RawRequest;
import dev.kotylu.model.response.ContentType;
import dev.kotylu.model.response.RawResponse;
import dev.kotylu.model.response.ResponseBuilder;
import dev.kotylu.status.StatusCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class ClientHandler extends Thread {
    private static Logger log = LoggerFactory.getLogger(ClientHandler.class);
    //private static int BUFFER_SIZE = 1;
    private Socket client;
    private InputStream in;
    private OutputStream out;

    public ClientHandler(Socket client) throws IOException {
        this.client = client;
        in = client.getInputStream();
        out = client.getOutputStream();
    }

    @Override
    public void run() {
        RawRequestParser requestParser = new RawRequestParser(in);
        try {
            RawRequest request = requestParser.get();
            log.info("INCOMING REQUEST: '{}'", request.toString());
            ResponseBuilder builder = new ResponseBuilder()
                .setStatusCode(StatusCode.OK)
                .setResponseBody(ContentType.TEXT_HTML_UTF8, "This is response from custom HTTP server");
            respond(builder.build());
            closeConnection();

        } catch (IOException e) {
            log.error("Unable to read from client request client: '{}'", client.getInetAddress().getHostName());
        }
    }

    private void respond(RawResponse response) throws IOException {
        out.write(response.toByteArray());
    }

    private void closeConnection() throws IOException {
        client.close();
    }
}
