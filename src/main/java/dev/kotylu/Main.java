package dev.kotylu;


import org.slf4j.*;
import java.net.*;
import java.io.*;

public class Main {
    private static Logger log = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        int PORT = 8080;
        try (ServerSocket server = new ServerSocket(PORT)) {
            while (true) {
                ClientHandler clientHandler = new ClientHandler(server.accept());
                clientHandler.start();
            }
        } catch (IOException e) {
            log.error("Connection was terminated");
        }
    }


}

