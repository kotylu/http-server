package dev.kotylu.status;

public class StatusCode {
    public final static StatusCode INTERNAL_ERROR = new StatusCode(500, "Internal Error");
    public final static StatusCode FORBIDDEN = new StatusCode(403, "Forbidden");
    public final static StatusCode UNAUTHORIZED = new StatusCode(401, "Unauthorized");
    public final static StatusCode OK = new StatusCode(200, "OK");

    private final int code;
    private final String reason;
    private StatusCode(int code, String reason) {
        this.code = code;
        this.reason = reason;
    }

    public String getStrCode() {
        return String.valueOf(code);
    }

    public String getReason() {
        return reason;
    }
}
