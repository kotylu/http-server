package dev.kotylu.datastruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.Charset;

public class ByteArray {
    private final static Logger log = LoggerFactory.getLogger(ByteArray.class);
    private final byte MAX_UTF_TO_BYTE = 64/8; // UTF is in bits but array is in bytes => /8
    // HTTP standard sets line limit to 998 + CRLF
    // https://datatracker.ietf.org/doc/html/rfc2822#page-6
    private int BUFFER_SIZE = 256;
    private byte[] data;
    private int length;

    public ByteArray() {
        length = 0;
        data = new byte[BUFFER_SIZE];
    }

    public int length() {
        return length;
    }

    public void add(byte value) {
        if (length == BUFFER_SIZE) {
            resize();
        }
        data[length++] = value;
    }

    public byte get(int ix) throws ArrayIndexOutOfBoundsException {
        if (ix < 0 || ix >= this.length) {
            throw new ArrayIndexOutOfBoundsException();
        }
        return data[ix];
    }

    public byte pop() {
        return data[--length];
    }

    public void clear() {
        length = 0;
    }

    private void resize() {
        BUFFER_SIZE *= 2;
        byte[] resized = new byte[BUFFER_SIZE];
        System.arraycopy(data, 0, resized, 0, length);
        data = resized;
    }

    @Override
    public String toString() {
        return new String(data).substring(0, length);
    }

    public String getStringFromRange(int inclusiveStartIX, int exclusiveEndIX, Charset charset) {
        log.warn("{} {}", inclusiveStartIX, exclusiveEndIX);
        if (inclusiveStartIX < 0 || inclusiveStartIX > length-1) {
            log.error("inclusiveStartIX({}) out of bound - array size {}", inclusiveStartIX, length);;
            throw new ArrayIndexOutOfBoundsException();
        }
        if (exclusiveEndIX < 0 || exclusiveEndIX > length) {
            log.error("exclusiveEndIX({}) out of bound - array size {}", exclusiveEndIX, length);;
            throw new ArrayIndexOutOfBoundsException();
        }
        if (exclusiveEndIX <= inclusiveStartIX) {
            log.error("exclusiveEndIX({}) <= InclusiveStartIX({})", exclusiveEndIX, inclusiveStartIX);
            throw new ArrayIndexOutOfBoundsException();
        }
        byte[] temp = new byte[exclusiveEndIX-inclusiveStartIX];
        System.arraycopy(data, inclusiveStartIX, temp, 0, exclusiveEndIX-inclusiveStartIX);
        return new String(temp, charset);
    }

    public char lastChar(Charset charset) {
        byte[] temp = new byte[Integer.min(length, MAX_UTF_TO_BYTE)];
        System.arraycopy(data, Integer.max(0, length - MAX_UTF_TO_BYTE), temp, 0, temp.length);
        char[] lastChars = new String(temp, charset).toCharArray();
        return lastChars[lastChars.length-1];
    }
}
