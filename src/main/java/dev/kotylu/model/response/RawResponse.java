package dev.kotylu.model.response;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class RawResponse {
    private final String statusLine;
    private final String headers;
    private String body;
    private Charset bodyEncoding;

    /**
     *
     * @param statusLine String containing status line with no break lines
     * @param headers Headers should already contain break lines after each (last including) header
     */
    protected RawResponse(String statusLine, String headers) {
        this.statusLine = statusLine + "\r\n"/* breaks status line */;
        this.headers = headers + "\r\n"/* creates empty line to signal end of header section*/;
    }

    protected void setBody(String body, Charset bodyEncoding) {
        this.body = body;
        this.bodyEncoding = bodyEncoding;
    }


    public byte[] toByteArray() {
        int bodyLength = body == null ? 0 : body.length();
        byte[] response = new byte[statusLine.length() + headers.length() + bodyLength];
        System.arraycopy(statusLine.getBytes(StandardCharsets.US_ASCII), 0 , response, 0, statusLine.length());
        System.arraycopy(headers.getBytes(StandardCharsets.US_ASCII), 0, response, statusLine.length(), headers.length());
        if (bodyLength > 0) {
            System.arraycopy(body.getBytes(bodyEncoding), 0, response, statusLine.length() + headers.length(), bodyLength);
        }

        return response;
    }
}
