package dev.kotylu.model.response;

import dev.kotylu.status.StatusCode;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class ResponseBuilder {
    private final static String HTTP_VERSION = "HTTP/1.1";
    private final static String SERVER_NAME = "CUSTOM";
    private String[] statusLine;
    private Map<String, String> headers;
    private ContentType contentType;
    private String body;

    public ResponseBuilder() {
        statusLine = new String[3]; // protocol, code, reason
        headers = new HashMap<>();
        statusLine[0] = HTTP_VERSION;
        //--- preset some headers
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss");
        headers = new HashMap<>();
        headers.put("Date", sdf.format(cal.getTime()));
        headers.put("Server", SERVER_NAME);
    }

    public ResponseBuilder setStatusCode(StatusCode status) {
        statusLine[1] = status.getStrCode();
        statusLine[2] = status.getReason();
        return this;
    }

    public ResponseBuilder setRawHeader(String headerName, String headerValue) {
        this.headers.put(headerName, headerValue);
        return this;
    }

    public ResponseBuilder setResponseBody(ContentType contentType, String body) {
        this.headers.put("Content-Type", contentType.toString());
        this.contentType = contentType;
        this.body = body;
        return this;
    }

    public RawResponse build() {
        String statusLineStr = String.join(" ", statusLine);
        StringBuilder headerBuilder = new StringBuilder();
        for (Map.Entry<String, String> headerEntry : headers.entrySet()) {
            headerBuilder.append(headerEntry.getKey());
            headerBuilder.append(": ");
            headerBuilder.append(headerEntry.getValue());
            headerBuilder.append("\r\n");
        }
        RawResponse response = new RawResponse(statusLineStr, headerBuilder.toString());
        if (body != null) {
            response.setBody(body, contentType.getEncoding());
        }
        return response;
    }

}
