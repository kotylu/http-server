package dev.kotylu.model.response;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class ContentType {
    public final static ContentType TEXT_HTML_UTF8 = new ContentType("text/html");

    private final String type;
    private final Charset encoding;

    private ContentType(String type, Charset encoding) {
        this.type = type;
        this.encoding = encoding;
    }

    private ContentType(String type) {
        this(type, StandardCharsets.UTF_8);
    }

    protected Charset getEncoding() {
        return encoding;
    }

    @Override
    public String toString() {
        return type+"; charset="+encoding.displayName();
    }
}
