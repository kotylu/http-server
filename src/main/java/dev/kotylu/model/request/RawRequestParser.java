package dev.kotylu.model.request;

import dev.kotylu.datastruct.ByteArray;

import java.nio.charset.*;
import org.slf4j.*;
import java.io.*;

public class RawRequestParser {
    private final static Logger log = LoggerFactory.getLogger(RawRequestParser.class);
    private final ByteArray buffer;
    private final InputStream in;
    private final RawRequest request;
    public RawRequestParser(InputStream in) {
        buffer = new ByteArray();
        this.in = in;
        request = new RawRequest();
    }

    public RawRequest get() throws IOException {
        int methodEnd = indexOfNextChar(' ', StandardCharsets.US_ASCII);
        int pathEnd = indexOfNextChar(' ', StandardCharsets.US_ASCII);
        int CRofFirstLine = indexOfNextChar('\r', StandardCharsets.US_ASCII);
        request.setMethod(buffer.getStringFromRange(0, methodEnd, StandardCharsets.US_ASCII));
        request.setPath(buffer.getStringFromRange(methodEnd+1, pathEnd, StandardCharsets.US_ASCII));
        request.setProtocol(buffer.getStringFromRange(pathEnd+1, CRofFirstLine, StandardCharsets.US_ASCII));
        int headerStartIX = CRofFirstLine+2; // skip 'CRLF' characters
        int[] headerLine;
        while ((headerLine = indexesOfMultipleChars(new char[]{':', '\r'}, StandardCharsets.US_ASCII))[0] >= 0) {
            String headerKey = buffer.getStringFromRange(headerStartIX, headerLine[0], StandardCharsets.US_ASCII);
            String headerValue = buffer.getStringFromRange(headerLine[0]+2/*skip ': '*/, headerLine[1], StandardCharsets.US_ASCII);
            headerStartIX = headerLine[1]+2; // skip 'CRLF' chars
            request.putHeader(headerKey, headerValue);
        }
        return request;
    }

    /**
     * Searches for current char and next char, if next char is found
     * before the current one - current one is set to -1 and is skipped
     * @param indexesOf array of characters to found in order
     * @param decoder byte-2-char charset decoder
     * @return array of indexes in order of found chars (-1 for skipped/not found)
     * @throws IOException
     */

    private int[] indexesOfMultipleChars(char[] indexesOf, Charset decoder) throws IOException {
        int value = 0;
        int[] found = new int[indexesOf.length];
        int foundIX = 0;
        while ((value = in.read()) >= 0) {
            buffer.add((byte)(value & 0xFF));
            char _char = buffer.lastChar(decoder);
            // NEXT IX
            if (foundIX < indexesOf.length-1 && indexesOf[foundIX+1] == _char) {
                found[foundIX] = -1;
                found[++foundIX] = buffer.length()-1;
                foundIX++;
            }
            // CURRENT IX
            else if (indexesOf[foundIX] == _char) {
                found[foundIX++] = buffer.length()-1;
            }
            // BREAKER (break on IX == LENGTH because it is already incremented)
            if (foundIX == indexesOf.length) {
                return found;
            }
        }
        return found;
    }

    private int indexOfNextChar(char indexOf, Charset decoder) throws IOException {
        int value = 0;
        while ((value = in.read()) >= 0) {
            buffer.add((byte)(value & 0xFF));
            char _char = buffer.lastChar(decoder);
            if (_char == indexOf) {
                return buffer.length() - 1;
            }
        }
        return -1;
    }
}
