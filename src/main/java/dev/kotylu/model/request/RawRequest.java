package dev.kotylu.model.request;

import java.util.HashMap;
import java.util.Map;

public class RawRequest {
    private String method;
    private String path;
    private String protocol;
    private Map<String, String> headers;
    private String body;

    public RawRequest() {
        this.headers = new HashMap<>();
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void putHeader(String key, String value) {
        headers.put(key, value);
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("METHOD: ");
        sb.append(method);
        sb.append('\n');
        sb.append("PATH: ");
        sb.append(path);
        sb.append('\n');
        sb.append("VERSION: ");
        sb.append(protocol);
        sb.append('\n');
        sb.append("HEADERS:");
        sb.append("\n");
        for (Map.Entry<String, String> pair : headers.entrySet()) {
            sb.append(pair.getKey());
            sb.append(" : ");
            sb.append(pair.getValue());
            sb.append("\n");
        }
        sb.append("\n");
        sb.append(body);
        return sb.toString();
    }
}